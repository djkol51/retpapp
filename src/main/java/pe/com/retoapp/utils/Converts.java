package pe.com.retoapp.utils;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;

import pe.com.retoapp.bean.TipoCambioBean;
import pe.com.retoapp.request.TipoCambioRequest;

public class Converts {
	
	
	public List<TipoCambioBean> toListTipoCambioResponse(List<TipoCambioBean> list){
		return list
				.stream()
				.map(this::toTipoCambioResponse)
				.collect(Collectors.toList());
	}
	
	
	private TipoCambioBean toTipoCambioResponse(TipoCambioBean tp) {
		TipoCambioBean response = new TipoCambioBean();
		BeanUtils.copyProperties(tp, response);
		return response;
	}

	
	public static TipoCambioRequest toTipoCambioRequest(int id, TipoCambioRequest req) {
		TipoCambioRequest request = new TipoCambioRequest();
		BeanUtils.copyProperties(req, request);
		request.setId(id);
		return request;
	}
}
