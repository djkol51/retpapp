package pe.com.retoapp.repository;

import org.springframework.stereotype.Repository;

import pe.com.retoapp.entities.TipoCambio;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface TipoCambioRepository extends JpaRepository<TipoCambio,Integer>{
}
