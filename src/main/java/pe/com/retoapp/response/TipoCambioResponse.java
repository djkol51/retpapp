package pe.com.retoapp.response;

import lombok.Data;

@Data
public class TipoCambioResponse {
	
	private Double monto;
	private Double montoTipoDeCambio;
	private String monedaOrigen;
	private String monedaDestino;
	private Double tipoDeCambio;

}
