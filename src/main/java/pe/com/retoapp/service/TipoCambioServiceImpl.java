package pe.com.retoapp.service;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import io.reactivex.Completable;
import io.reactivex.Single;
import pe.com.retoapp.bean.TipoCambioBean;
import pe.com.retoapp.entities.TipoCambio;
import pe.com.retoapp.repository.TipoCambioRepository;
import pe.com.retoapp.request.CambioRequest;
import pe.com.retoapp.request.TipoCambioRequest;
import pe.com.retoapp.response.TipoCambioResponse;

@Service
public class TipoCambioServiceImpl implements TipoCambioService {

	@Autowired
	private TipoCambioRepository tipoCambioRepository;

	@Override
	public Single<Integer> addTipoCambio(TipoCambioRequest addTipoCambioRequest) {
		return saveTipoCambio(addTipoCambioRequest);
	}

	@Override
	public Single<List<TipoCambioBean>> getAllTipoCambio(int limit, int page) {
		return findAllTipoCambioInRepository(limit, page)
				.map(this::listTipoCambio);
	}
	

	@Override
	public Completable updateTipoCambio(TipoCambioRequest addTipoCambioRequest) {
		return upTipoCambio(addTipoCambioRequest);
	}
	
	
	@Override
	public Single<TipoCambioResponse> convertTipoCambio(CambioRequest request) {
		return processToTipoCambio(request);
	}
	
	
	/** list **/
	
	private Single<List<TipoCambio>> findAllTipoCambioInRepository(int limit, int page){
		return Single.create(suscribe ->{
			List<TipoCambio> list = tipoCambioRepository.findAll(
					PageRequest.of(page, limit)).getContent();
			suscribe.onSuccess(list);
		});
	}
	
	
	private List<TipoCambioBean> listTipoCambio(List<TipoCambio> list){
		return list
				.stream()
				.map(this::tipoCambioConvertResponse)
				.collect(Collectors.toList());
	}
	
	/**  **/
	
	private Single<TipoCambioResponse> processToTipoCambio(CambioRequest request){
		
		return Single.create(suscriptor ->{
			List<TipoCambio> tipoCambio = tipoCambioRepository.findAll();
			double cambio = 0;
			
			if(tipoCambio.isEmpty()) {
				suscriptor.onError(new EntityNotFoundException());
			}else {
				
				for(TipoCambio obj : tipoCambio) {
					if(obj.getTipoMonedaDestino().contains(request.getMonedaDestino())) {
						cambio = obj.getValorTipoCambio();
						break;
					}
				}
				
				DecimalFormat format = new DecimalFormat("####.##");
				double montoConTipoCambio = request.getMonto() * cambio;
				
				TipoCambioResponse response = new TipoCambioResponse();
				
				response.setMonto(request.getMonto());
				response.setMontoTipoDeCambio(Double.valueOf(format.format(montoConTipoCambio)));
				response.setMonedaOrigen(request.getMonedaOrigen());
				response.setMonedaDestino(request.getMonedaDestino());
				response.setTipoDeCambio(cambio);
				
				suscriptor.onSuccess(response);
				
			}
			
		});
		
		
	}
	
	
	/** save data **/
	
	private Single<Integer> saveTipoCambio(TipoCambioRequest tipoCambioRequest){
		return Single.create(suscriptor ->{
			int id = tipoCambioRepository.save(convertTipoCambioEntity(tipoCambioRequest))
					.getIdTipoCambio();
			suscriptor.onSuccess(id);
		});
	}
	
	
	/**  update data  **/
	
	private Completable upTipoCambio(TipoCambioRequest request) {
		return Completable.create(suscriptor ->{
			Optional<TipoCambio> optionalTipoCambio = tipoCambioRepository.findById(request.getId());
			if(!optionalTipoCambio.isPresent()) {
				suscriptor.onError(new EntityNotFoundException());
			}else {
				TipoCambio tp = optionalTipoCambio.get();
				tipoCambioRepository.save(tp);
				suscriptor.onComplete();
			}
		});
	}
	
	
	/** Converts **/
	
	private TipoCambioBean tipoCambioConvertResponse(TipoCambio tipoCambio) {
		TipoCambioBean response = new TipoCambioBean();
		BeanUtils.copyProperties(tipoCambio, response);
		
		response.setIdTipoCambio(tipoCambio.getIdTipoCambio());
		response.setTipoMonedaOrigen(tipoCambio.getTipoMonedaOrigen());
		response.setTipoMonedaDestino(tipoCambio.getTipoMonedaDestino());
		response.setValorTipoCambio(tipoCambio.getValorTipoCambio());
		
		return response;
	}
	
	

	private TipoCambio convertTipoCambioEntity(TipoCambioRequest tipoCambioRequest) {
		TipoCambio tp = new TipoCambio();
		BeanUtils.copyProperties(tipoCambioRequest, tp);
		tp.setTipoMonedaOrigen(tipoCambioRequest.getMonedaOrigen());
		tp.setTipoMonedaDestino(tipoCambioRequest.getMonedaDestino());
		tp.setValorTipoCambio(tipoCambioRequest.getValorTipoCambio());
		return tp;
	}

	
}
