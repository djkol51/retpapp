package pe.com.retoapp.service;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import pe.com.retoapp.bean.TipoCambioBean;
import pe.com.retoapp.request.CambioRequest;
import pe.com.retoapp.request.TipoCambioRequest;
import pe.com.retoapp.response.TipoCambioResponse;

public interface TipoCambioService {
	
	Single<Integer> addTipoCambio(TipoCambioRequest addTipoCambioRequest);
	
	Single<List<TipoCambioBean>> getAllTipoCambio(int limit, int page);
	
	Completable updateTipoCambio(TipoCambioRequest addTipoCambioRequest);
	
	Single<TipoCambioResponse> convertTipoCambio(CambioRequest request);

}
