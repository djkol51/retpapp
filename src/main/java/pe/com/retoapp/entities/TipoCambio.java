package pe.com.retoapp.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "tipo_cambio")
@Data
public class TipoCambio {

	@Id
	@Column(name = "id_tipo_cambio")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idTipoCambio;

	@Column(name = "tipo_moneda_origen")
	private String tipoMonedaOrigen;
	
	@Column(name = "tipo_moneda_destino")
	private String tipoMonedaDestino;

	@Column(name = "valor_tipo_cambio")
	private Double valorTipoCambio;
	
//	@Column(name = "valor_cambio_compra")
//	private Double valorCambioCompra;
	
//	@Column(name = "valor_cambio_venta")
//	private Double valorCambioVenta;


}
