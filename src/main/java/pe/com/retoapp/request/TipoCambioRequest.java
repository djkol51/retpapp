package pe.com.retoapp.request;

import lombok.Data;

@Data
public class TipoCambioRequest {
	
	private Integer id;
	private Double valorTipoCambio;
	private String monedaOrigen;
	private String monedaDestino;

}
