package pe.com.retoapp.request;

import lombok.Data;

@Data
public class CambioRequest {
	
	private Double monto;
	private String monedaOrigen;
	private String monedaDestino;
	

}
