package pe.com.retoapp.bean;

import java.io.Serializable;


import lombok.Data;

@Data
public class TipoCambioBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private int idTipoCambio;

	private String tipoMonedaOrigen;
	
	private String tipoMonedaDestino;

	private Double valorTipoCambio;

	
}
