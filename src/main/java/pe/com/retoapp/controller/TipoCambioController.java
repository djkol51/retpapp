package pe.com.retoapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import pe.com.retoapp.bean.TipoCambioBean;
import pe.com.retoapp.request.CambioRequest;
import pe.com.retoapp.request.TipoCambioRequest;
import pe.com.retoapp.response.BaseWebResponse;
import pe.com.retoapp.service.TipoCambioService;
import pe.com.retoapp.utils.Converts;

@RestController
@RequestMapping(value = "/tipoCambio")
public class TipoCambioController {

	@Autowired
	private TipoCambioService tipoCambioService;
	
	
	 @PostMapping(
			 	value = "add",
	            consumes = MediaType.APPLICATION_JSON_VALUE,
	            produces = MediaType.APPLICATION_JSON_VALUE
	    )
	 public Single<ResponseEntity<BaseWebResponse>> addTipoCambio(
			 @RequestBody TipoCambioRequest req){
		 return tipoCambioService.addTipoCambio(req).subscribeOn(Schedulers.io())
				 .map(res -> ResponseEntity.ok(BaseWebResponse.successWithData(res)));
		 
	 }
	 
	 
	 @GetMapping(value = "list",
	            produces = MediaType.APPLICATION_JSON_VALUE
	    )
	 public Single<ResponseEntity<BaseWebResponse<List<TipoCambioBean>>>> getAllTipoCambio(
			 @RequestParam(value = "limit", defaultValue = "10")int limit,
			 @RequestParam(value = "page", defaultValue = "0")int page){
		 return tipoCambioService.getAllTipoCambio(limit, page)
				 .subscribeOn(Schedulers.io())
				 .map(res -> ResponseEntity.ok(BaseWebResponse.successWithData(res)));
	 }
	 
	 
	 @PutMapping(
	            value = "update/{id}",
	            consumes = MediaType.APPLICATION_JSON_VALUE,
	            produces = MediaType.APPLICATION_JSON_VALUE
	    )
	 public Single<ResponseEntity<BaseWebResponse>> updateTipoCambio(@PathVariable(value = "id") int id,
			 @RequestBody TipoCambioRequest request){
		 return tipoCambioService.updateTipoCambio(Converts.toTipoCambioRequest(id, request))
				 .subscribeOn(Schedulers.io())
				 .toSingle(() -> ResponseEntity.ok(BaseWebResponse.successNoData()));
	 }
	 
	 
	 @PostMapping(value = "convert",
			 consumes = MediaType.APPLICATION_JSON_VALUE,
	            produces = MediaType.APPLICATION_JSON_VALUE)
	 public Single<ResponseEntity<BaseWebResponse>> toTipoCambio(@RequestBody CambioRequest request){
		 return tipoCambioService.convertTipoCambio(request)
				 .subscribeOn(Schedulers.io())
				 .map(res -> ResponseEntity.ok(BaseWebResponse.successWithData(res)));
	 }
	 
	
}
