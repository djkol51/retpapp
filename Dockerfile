FROM adoptopenjdk:11-jre-hotspot

WORKDIR /app

COPY target/RetoApp-*.jar RetoApp.jar

EXPOSE 8081

CMD [ "java", "-jar", “RetoApp.jar” ]